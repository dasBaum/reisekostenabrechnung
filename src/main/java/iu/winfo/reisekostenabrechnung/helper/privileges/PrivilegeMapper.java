package iu.winfo.reisekostenabrechnung.helper.privileges;

import iu.winfo.reisekostenabrechnung.models.security.Privilege;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public class PrivilegeMapper {
    public static String getDisplayText(Privilege privilege){
        return switch (privilege.getName()) {
            case "userRead" -> "Nutzer auslesen";
            case "userWrite" -> "Nutzer anlegen";
            case "userDelete" -> "Nutzer löschen";
            case "roleRead" -> "Rollen auslesen";
            case "roleWrite" -> "Rollen anlegen";
            case "roleDelete" -> "Rollen löschen";
            case "travelCreate" -> "Reisekostenabrechnung erstellen";
            case "travelProcess" -> "Reisekostenabrechnung bearbeiten";
            case "travelProcessAll" -> "alle Reisekostenabrechnung bearbeiten";
            case "supervisor" -> "Vorgesetzter";
            default -> "Privilege cant be mapped";
        };
    }
}
