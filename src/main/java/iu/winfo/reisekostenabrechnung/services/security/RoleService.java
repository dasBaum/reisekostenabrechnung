package iu.winfo.reisekostenabrechnung.services.security;

import iu.winfo.reisekostenabrechnung.dto.role.RoleSaveRequest;
import iu.winfo.reisekostenabrechnung.exceptions.RoleAlreadyExistsException;
import iu.winfo.reisekostenabrechnung.interfaces.IPrivilegeService;
import iu.winfo.reisekostenabrechnung.interfaces.IRoleService;
import iu.winfo.reisekostenabrechnung.models.security.Privilege;
import iu.winfo.reisekostenabrechnung.models.security.Role;
import iu.winfo.reisekostenabrechnung.repositories.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Service
public class RoleService implements IRoleService {

    private final Logger _logger = LoggerFactory.getLogger(RoleService.class);
    private final RoleRepository _roleRepository;
    private final IPrivilegeService _privilegeService;

    @Autowired
    public RoleService(RoleRepository roleRepository, IPrivilegeService privilegeService) {
        _roleRepository = roleRepository;
        _privilegeService = privilegeService;
    }


    @Override
    public Role update(Role role) {
        Role updatedRole = _roleRepository.save(role);
        _logger.info(role.getName() + " was updated");
        return updatedRole;
    }

    @Override
    public Role update(RoleSaveRequest inputRole) {
        Optional<Role> prevRole = Optional.empty();
        Role role = new Role();
        if(inputRole.getPrevRoleId().isPresent()){
            prevRole = _roleRepository.findById(inputRole.getPrevRoleId().get());
            prevRole.ifPresent(pr -> role.setId(pr.getId()));
        }
        if(prevRole.isEmpty() && _roleRepository.findByName(inputRole.getName()).isPresent()){
            throw new RoleAlreadyExistsException("There is already a role with name " + inputRole.getName());
        }
        if(!inputRole.getName().equals("")){
            role.setName(inputRole.getName());
        }
        if(inputRole.getPower() > 0){
            role.setPower(inputRole.getPower());
        }
        role.setPrivileges(inputRole.getPrivilegeList());
        if(role.getPrivileges().stream().anyMatch(privilege -> privilege.getName().contains("travel"))){
            Optional<Privilege> travelExpensePrivilege = _privilegeService.get("travelExpense");
            travelExpensePrivilege.ifPresent(privilege -> role.getPrivileges().add(privilege));
        }

        Role updatedRole = update(role);
        _logger.info("role with name " + role.getName() + " was updated.");
        return updatedRole;
    }

    @Override
    public Optional<Role> get(UUID id) {
        return _roleRepository.findById(id);
    }

    @Override
    public void delete(UUID id) {
        if(_roleRepository.findById(id).isEmpty()) return;
        Role role = _roleRepository.findById(id).get();
        if (!role.getName().equals("Admin")) {
            _roleRepository.deleteById(id);
        }
        _logger.info(role.getName() + " was deleted");
    }

    @Override
    public Page<Role> list(Pageable pageable) {
        return _roleRepository.findAll(pageable);
    }

    @Override
    public List<Role> list() {
        return _roleRepository.findAll();
    }

    @Override
    public long count() {
        return _roleRepository.count();
    }

    @Override
    public Optional<Role> get(String name) {
        return _roleRepository.findByName(name);
    }
}
