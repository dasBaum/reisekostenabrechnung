package iu.winfo.reisekostenabrechnung.services.security;

import iu.winfo.reisekostenabrechnung.interfaces.IPrivilegeService;
import iu.winfo.reisekostenabrechnung.models.security.Privilege;
import iu.winfo.reisekostenabrechnung.repositories.PrivilegeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Service
public class PrivilegeService implements IPrivilegeService {

    private final PrivilegeRepository _privilegeRepository;
    private final Logger _logger;

    public PrivilegeService(PrivilegeRepository privilegeRepository) {
        _privilegeRepository = privilegeRepository;
        _logger = LoggerFactory.getLogger(PrivilegeService.class);
    }

    @Override
    public Privilege update(Privilege privilege) {
        _privilegeRepository.save(privilege);
        _logger.info(privilege.getName() + " was updated");
        return privilege;
    }

    @Override
    public Optional<Privilege> get(UUID id) {
        return _privilegeRepository.findById(id);
    }

    @Override
    public Optional<Privilege> get(String name) {
        return _privilegeRepository.findByName(name);
    }

    @Override
    public void delete(UUID id) {
        Optional<Privilege> maybePrivilege = this.get(id);
        if (maybePrivilege.isPresent()) {
            _privilegeRepository.delete(maybePrivilege.get());
            _logger.info(maybePrivilege.get().getName() + " was deleted.");
        } else {
            _logger.info("tried to delete privilege with id " + id.toString() + " but it isn't existent");
        }
    }

    @Override
    public Page<Privilege> list(Pageable pageable) {
        return _privilegeRepository.findAll(pageable);
    }

    @Override
    public List<Privilege> list() {
        return _privilegeRepository.findAll();
    }

    @Override
    public long count() {
        return _privilegeRepository.count();
    }
}
