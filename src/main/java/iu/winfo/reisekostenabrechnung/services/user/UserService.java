package iu.winfo.reisekostenabrechnung.services.user;

import iu.winfo.reisekostenabrechnung.dto.user.UserSaveRequest;
import iu.winfo.reisekostenabrechnung.exceptions.UserAlreadyExistsException;
import iu.winfo.reisekostenabrechnung.interfaces.IUserService;
import iu.winfo.reisekostenabrechnung.models.security.User;
import iu.winfo.reisekostenabrechnung.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Service
public class UserService implements IUserService {

    private final UserRepository _userRepository;
    private final PasswordEncoder _encoder;
    private final Logger _logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder encoder) {
        _userRepository = userRepository;
        _encoder = encoder;
    }

    @Override
    public User update(UserSaveRequest request) throws UserAlreadyExistsException {
        Optional<User> prevUser = Optional.empty();
        User user = new User();
        if (request.getUserId().isPresent()) {
            prevUser = _userRepository.findById(request.getUserId().get());
            prevUser.ifPresent(pu -> user.setId(pu.getId()));
        }

        if (!request.getUsername().equals("")) {
            user.setUsername(request.getUsername());
        }
        if (!request.getFirstName().equals("")) {
            user.setFirstName(request.getFirstName());
        }
        if (!request.getLastName().equals("")) {
            user.setLastName(request.getLastName());
        }
        if (!request.getEmail().equals("")) {
            if(prevUser.isPresent()){
            if (request.getEmail().equals(prevUser.get().getEmail())) {
                user.setEmail(request.getEmail());
            }
            } else {
                if (emailExist(request.getEmail())) {
                    _logger.warn("Tried to create user account with " + request.getEmail() + " but this email is already in use");
                    throw new UserAlreadyExistsException("There is already an account with this email address: " + request.getEmail());
                }
                user.setEmail(request.getEmail());
            }
        }
        if (!request.getPassword().equals("") && request.getPassword().equals(request.getMatchingPassword())) {
            user.setPasswordHash(_encoder.encode(request.getPassword()));
        } else {
            if (request.getUserId().isPresent()) {
                prevUser.ifPresent(value -> user.setPasswordHash(value.getPasswordHash()));
            }
        }
        if (request.getUserId().isPresent()) {
            user.setId(request.getUserId().get());
        }
        user.setRoles(request.getRoleList());
        User result = update(user);
        _logger.info("User with username " + user.getUsername() + " was updated");
        return result;
    }

    private boolean emailExist(String email) {
        return _userRepository.findByEmail(email) != null;
    }


    public Optional<User> get(UUID id) {
        return _userRepository.findById(id);
    }

    public User update(User user) {
        return _userRepository.save(user);
    }

    public void delete(UUID id) {
        _userRepository.deleteById(id);
    }

    public List<User> list() {
        return _userRepository.findAll();
    }

    public Page<User> list(Pageable pageable) {
        return _userRepository.findAll(pageable);
    }

    public long count() {
        return _userRepository.count();
    }

}
