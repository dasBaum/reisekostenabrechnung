package iu.winfo.reisekostenabrechnung.services.travel;

import iu.winfo.reisekostenabrechnung.interfaces.ITravelService;
import iu.winfo.reisekostenabrechnung.models.documents.InvoiceDocument;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpenses;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpensesEntry;
import iu.winfo.reisekostenabrechnung.repositories.InvoiceDocumentRepository;
import iu.winfo.reisekostenabrechnung.repositories.TravelExpensesEntryRepository;
import iu.winfo.reisekostenabrechnung.repositories.TravelExpensesRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Service
public class TravelService implements ITravelService {

    private final TravelExpensesRepository _travelExpenseRepository;
    private final TravelExpensesEntryRepository _travelExpenseEntryRepository;

    private final InvoiceDocumentRepository _invoiceDocumentRepository;

    public TravelService(TravelExpensesRepository travelExpensesRepository, TravelExpensesEntryRepository travelExpensesEntryRepository, InvoiceDocumentRepository invoiceDocumentRepository){
        _invoiceDocumentRepository = invoiceDocumentRepository;
        _travelExpenseRepository = travelExpensesRepository;
        _travelExpenseEntryRepository = travelExpensesEntryRepository;
    }
    @Override
    public TravelExpenses updateExpense(TravelExpenses travelExpenses) {
        return _travelExpenseRepository.save(travelExpenses);
    }

    @Override
    public Optional<TravelExpenses> findExpense(UUID id) {
        return Optional.empty();
    }

    @Override
    public void deleteExpense(UUID id) {
    }

    @Override
    public List<TravelExpenses> listExpenses() {
        return _travelExpenseRepository.findAll();
    }

    @Override
    public Page<TravelExpenses> listExpenses(Pageable pageable) {
        return null;
    }

    @Override
    public TravelExpensesEntry updateEntry(TravelExpensesEntry expensesEntry) {
        TravelExpensesEntry entry = _travelExpenseEntryRepository.save(expensesEntry);
        TravelExpenses expenses = entry.getTravelExpenses();
        expenses.setTotalPrice(_travelExpenseRepository.getTotalPriceByExpense(expenses.getId()));
        updateExpense(expenses);
        return entry;
    }

    @Override
    public Optional<TravelExpensesEntry> findEntry(UUID id) {
        return Optional.empty();
    }



    @Override
    public void deleteEntry(UUID id) {

    }

    @Override
    public List<TravelExpensesEntry> listEntries() {
        return null;
    }

    @Override
    public Page<TravelExpensesEntry> listEntries(Pageable pageable) {
        return null;
    }

    @Override
    public Set<TravelExpensesEntry> getEntriesByExpense(UUID id) {
        return _travelExpenseEntryRepository.findByTravelExpenses_IdEquals(id);
    }

    @Override
    public Set<TravelExpenses> getByUserId(UUID id) {
        return _travelExpenseRepository.findByUser_IdEquals(id);
    }

    @Override
    public Optional<InvoiceDocument> findInvoiceDocByEntryId(UUID id) {
        return _invoiceDocumentRepository.findByTravelExpensesEntry_IdEquals(id);
    }
}
