package iu.winfo.reisekostenabrechnung.views.roles;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.SerializableBiConsumer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import iu.winfo.reisekostenabrechnung.helper.security.AuthenticatedUser;
import iu.winfo.reisekostenabrechnung.interfaces.IRoleService;
import iu.winfo.reisekostenabrechnung.models.security.Privilege;
import iu.winfo.reisekostenabrechnung.models.security.Role;
import iu.winfo.reisekostenabrechnung.models.security.User;
import iu.winfo.reisekostenabrechnung.views.MainLayout;
import iu.winfo.reisekostenabrechnung.views.components.role.RoleCreate;

import javax.annotation.security.RolesAllowed;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Route(value = "roles", layout = MainLayout.class)
@PageTitle("Rollen verwalten")
@RolesAllowed("roleRead")
public class RoleIndex extends VerticalLayout {
    private IRoleService _roleService;
    private final AuthenticatedUser _authenticatedUser;
    private Optional<User> invokingUser;
    private RoleCreate _roleCreate;
    private Button createButton;
    private Grid<Role> grid;
    private final SerializableBiConsumer<HorizontalLayout, Role> updateGridButtonsConsumer = (layout, role) -> {
        if(invokingUser.isEmpty()) return;
        List<String> privileges = new ArrayList<>();
        for (Role userRole: invokingUser.get().getRoles()) {
            privileges.addAll(userRole.getPrivileges().stream().map(Privilege::getName).filter(name -> !privileges.contains(name)).toList());
        }
        Button updateButton = new Button(new Icon(VaadinIcon.COG_O));

        updateButton.addClickListener(e ->{
            if(!_roleCreate.isOpened()){
                _roleCreate.initComponents(role);
                _roleCreate.open();
            }
        });
        updateButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button deleteButton = new Button(new Icon(VaadinIcon.TRASH));
        deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        deleteButton.addClickListener(e -> {
            _roleService.delete(role.getId());
            updateGrid();
        });
        if (privileges.stream().anyMatch(privilege -> privilege.equals("roleWrite")) && privileges.stream().anyMatch(privilege -> privilege.equals("roleDelete"))) {
            layout.add(updateButton, deleteButton);
        } else if (privileges.stream().anyMatch(privilege -> privilege.equals("roleWrite"))) {
            layout.add(updateButton);
        }
    };

    public RoleIndex(IRoleService roleService, AuthenticatedUser authenticatedUser, RoleCreate roleCreate){
        _roleService = roleService;
        _authenticatedUser = authenticatedUser;
        _roleCreate = roleCreate;
        _roleCreate.addListener(RoleCreate.SavedEvent.class, e -> {
            updateGrid();
            _roleCreate.close();
        });
        prepareComponents();
    }

    private void prepareComponents() {
        this.setSizeFull();
        getInvokingUser();
        prepareGrid();
        prepareCreateButton();
        addComponents();
    }

    private void prepareCreateButton() {
        createButton = new Button("Rolle erstellen");
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        createButton.addClickListener(e ->{
            if(!_roleCreate.isOpened()){
                _roleCreate.initComponents(new Role());
                _roleCreate.open();
            }
        });
    }

    private void getInvokingUser() {
        invokingUser = _authenticatedUser.get();
    }

    private void addComponents(){
        if(invokingUser.isEmpty()) return;
        for (Role role : invokingUser.get().getRoles()) {
            if (role.getPrivileges().stream().anyMatch(privilege -> privilege.getName().equals("roleWrite")))
                add(createButton);
        }
        this.add(grid);
    }

    private void prepareGrid() {
        grid = new Grid<>(Role.class, false);
        grid.addClassName("role-grid");
        grid.addColumn(Role::getName).setHeader("Name").setAutoWidth(true);
        grid.addColumn(Role::getPower).setHeader("Level").setAutoWidth(true);
        grid.addColumn(createGridButtons()).setAutoWidth(true);
        updateGrid();
    }

    private void updateGrid(){
        List<Role> roleList = _roleService.list();
        grid.setItems(roleList);
    }

    private ComponentRenderer<HorizontalLayout, Role> createGridButtons(){
        return new ComponentRenderer<>(HorizontalLayout::new, updateGridButtonsConsumer);
    }
}
