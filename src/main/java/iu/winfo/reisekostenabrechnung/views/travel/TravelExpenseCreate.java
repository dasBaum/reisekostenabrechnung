package iu.winfo.reisekostenabrechnung.views.travel;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.SerializableBiConsumer;
import com.vaadin.flow.spring.annotation.UIScope;
import iu.winfo.reisekostenabrechnung.helper.security.AuthenticatedUser;
import iu.winfo.reisekostenabrechnung.interfaces.ITravelService;
import iu.winfo.reisekostenabrechnung.models.security.User;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpenses;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpensesEntry;
import iu.winfo.reisekostenabrechnung.views.components.travel.TravelExpensesEntryComp;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@UIScope
@Component
public class TravelExpenseCreate extends VerticalLayout {

    private final ITravelService _travelService;
    private TravelExpenses expenses;
    private TextField titel;

    private final Binder<TravelExpenses> binder = new Binder<>(TravelExpenses.class);

    private Button createButton;
    private Grid<TravelExpensesEntry> entryGrid;
    private TextArea description;

    private List<TravelExpensesEntry> entries;

    private NumberField totalPrice;
    private final Optional<User> invokingUser;

    private final TravelExpensesEntryComp _entryCreate;
    private Button saveButton;
    private Button cancelButton;

    private final SerializableBiConsumer<HorizontalLayout, TravelExpensesEntry> entryGridButtons = ((horizontalLayout, travelExpensesEntry) -> {
        Button deleteButton = new Button(new Icon(VaadinIcon.TRASH));
        deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        deleteButton.addClickListener(event -> {
            entries.remove(travelExpensesEntry);
            updateGrid();
        });
        horizontalLayout.add(deleteButton);
    });


    public TravelExpenseCreate(AuthenticatedUser authenticatedUser, ITravelService travelService, TravelExpensesEntryComp entryComp) {
        _travelService = travelService;
        invokingUser = authenticatedUser.get();
        _entryCreate = entryComp;
        expenses = new TravelExpenses();
        entries = new ArrayList<>();
        prepareComponents();
        addComponents();
        this.setMinWidth("33%");
    }

    private void updateGrid() {
        totalPrice.setValue(entries.stream().mapToDouble(TravelExpensesEntry::getPrice).sum());
        entryGrid.setItems(entries);
    }

    private void addComponents() {
        this.setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        this.add(titel,description,totalPrice);
        this.add(createButton);
        this.setHorizontalComponentAlignment(Alignment.START, createButton);
        this.add(entryGrid);
        HorizontalLayout buttonLayout = new HorizontalLayout(saveButton, cancelButton);
        buttonLayout.getStyle().set("flex-wrap", "wrap");
        buttonLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        buttonLayout.setWidthFull();
        _entryCreate.addListener(TravelExpensesEntryComp.SaveEvent.class, event -> {
            TravelExpensesEntry entry = event.getEntry();
            entry.setTravelExpenses(expenses);
            entries.add(entry);
            updateGrid();
        });
        this.add(buttonLayout);
    }

    private void prepareComponents() {
        prepareFields();
        prepareGrid();
        prepareButtons();
        prepareBinder();
    }

    private ComponentRenderer<HorizontalLayout, TravelExpensesEntry> renderEntryButtons() {
        return new ComponentRenderer<>(HorizontalLayout::new, entryGridButtons);
    }

    private void prepareBinder() {
        binder.forField(titel).asRequired("Es muss ein Titel angegeben werden.").withValidator(titelVal -> titelVal.length() >= 4, "Der Titel muss mehr als 4 Zeichen enthalten.").bind(TravelExpenses::getTitel, TravelExpenses::setTitel);
        binder.forField(description).asRequired("Es muss eine Beschreibung angegeben werden").withValidator(descriptionVal -> descriptionVal.length() >= 4, "Die Beschreibung muss länger als 4 Zeichen sein").bind(TravelExpenses::getDescription, TravelExpenses::setDescription);
        binder.forField(totalPrice).asRequired("Es muss ein Preis angegeben werden, füge Einträge hinzu!").withValidator(priveVal -> priveVal > 0, "Es muss ein Preis angegeben werden, füge Einträge hinzu!").bind(TravelExpenses::getTotalPrice, TravelExpenses::setTotalPrice);
        binder.readBean(expenses);
    }

    private void prepareButtons() {
        saveButton = new Button("Speichern");
        cancelButton = new Button("Abbrechen");
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        saveButton.getStyle().set("margin-inline-end", "auto");
        cancelButton.addClickListener(event -> this.clear());
        saveButton.addClickListener(event -> {
            try {
                binder.validate();
                saveExpense();
            } catch (ValidationException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void saveExpense() throws ValidationException {
        if(binder.isValid() && !entries.isEmpty() && invokingUser.isPresent()){
            expenses.setEntries(new HashSet<>(entries));
            expenses.setUser(invokingUser.get());
            binder.writeBean(expenses);
            _travelService.updateExpense(expenses);
            clear();
            prepareBinder();
        }
    }

    private void clear() {
        expenses = new TravelExpenses();
        titel.clear();
        description.clear();
        totalPrice.clear();
        entries = new ArrayList<>();
        updateGrid();
    }

    private void prepareFields() {
        titel = new TextField("Titel");
        titel.setMaxLength(50);
        titel.setMinWidth(33, Unit.PERCENTAGE);
        description = new TextArea("Beschreibung");
        description.setMaxLength(255);
        description.setMinWidth(33, Unit.PERCENTAGE);
        totalPrice = new NumberField("Gesamtpreis");
        totalPrice.setMinWidth(33, Unit.PERCENTAGE);
        totalPrice.setReadOnly(true);
        createButton = new Button("Eintrag erstellen");
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        createButton.addClickListener(event -> {
            _entryCreate.setTravelExpensesEntry(new TravelExpensesEntry());
            _entryCreate.open();
        });
    }

    private void prepareGrid() {
        entryGrid = new Grid<>(TravelExpensesEntry.class, false);
        entryGrid.addClassName("travel-expense-grid");
        entryGrid.addColumn(TravelExpensesEntry::getTitel).setHeader("Titel").setAutoWidth(false);
        entryGrid.addColumn(entry -> DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm").format(entry.getFromDate())).setHeader("Startdatum").setAutoWidth(false);
        entryGrid.addColumn(entry -> DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm").format(entry.getEndDate())).setHeader("Enddatum").setAutoWidth(false);
        entryGrid.addColumn(TravelExpensesEntry::getPrice).setHeader("Preis").setAutoWidth(false);
        entryGrid.addColumn(renderEntryButtons());
        updateGrid();
    }

}
