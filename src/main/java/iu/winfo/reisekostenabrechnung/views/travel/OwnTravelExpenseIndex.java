package iu.winfo.reisekostenabrechnung.views.travel;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.SerializableBiConsumer;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.spring.annotation.UIScope;
import iu.winfo.reisekostenabrechnung.helper.security.AuthenticatedUser;
import iu.winfo.reisekostenabrechnung.interfaces.ITravelService;
import iu.winfo.reisekostenabrechnung.models.documents.InvoiceDocument;
import iu.winfo.reisekostenabrechnung.models.security.User;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpenses;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpensesEntry;
import iu.winfo.reisekostenabrechnung.views.components.travel.TravelExpensesEntryComp;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@UIScope
@Component
public class OwnTravelExpenseIndex extends VerticalLayout {

    private final ITravelService _travelService;

    private final AuthenticatedUser _authenticatedUser;

    private Optional<User> invokingUser;

    private Grid<TravelExpenses> expensesGrid;

    private List<TravelExpenses> travelExpenses;
    private TravelExpensesEntryComp _entryComp = null;

    private final SerializableBiConsumer<HorizontalLayout, TravelExpensesEntry> entryGridButtons = (layout, entry) -> {
        Optional<InvoiceDocument> doc = Optional.ofNullable(entry.getInvoiceDocument());
        TravelExpenses tempExpense = entry.getTravelExpenses();
        Button updateButton = new Button(new Icon(VaadinIcon.COG_O));
        updateButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        _entryComp.addListener(TravelExpensesEntryComp.UpdateEvent.class, saveEvent -> {
            TravelExpensesEntry updatedEntry = saveEvent.getEntry();
            updatedEntry.setTravelExpenses(tempExpense);
            updateExpenseEntry(updatedEntry);
            updateGrid();
            _entryComp.setUpdateEntry(false);
        });
        updateButton.addClickListener(event -> {
            _entryComp.setTravelExpensesEntry(entry);
            _entryComp.setUpdateEntry(true);
            _entryComp.open();
        });
        layout.add(updateButton);
        if(doc.isPresent()){
            try {
                Anchor downloadLink;
                StreamResource streamResource = new StreamResource(doc.get().getFileName() + "." + doc.get().getFileExtension(), () -> new ByteArrayInputStream(doc.get().getBinaryData()));
                downloadLink = new Anchor(streamResource, "");
                downloadLink.getElement().setAttribute("download", true);
                downloadLink.add(new Button(new Icon(VaadinIcon.DOWNLOAD_ALT)));
                layout.add(downloadLink);
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    };

    private void updateExpenseEntry(TravelExpensesEntry entry) {
        _travelService.updateEntry(entry);
    }

    private final SerializableBiConsumer<VerticalLayout, TravelExpenses> detailsComponents = (layout, expense) -> {
        Tabs tabs = new Tabs();
        layout.setSizeFull();
        VerticalLayout detailsLayout = new VerticalLayout();
        Tab entryTab = new Tab("Abrechnungsposition");
        Tab detailsTab = new Tab("Details");
        VerticalLayout content = new VerticalLayout();
        content.setSizeFull();
        tabs.addSelectedChangeListener(event -> {
            content.removeAll();
            if (event.getSelectedTab().equals(detailsTab)) {
                TextField titelField = new TextField("Titel", expense.getTitel(), "Titel");
                titelField.setWidthFull();
                titelField.setReadOnly(true);
                TextArea descriptionField = new TextArea("Beschreibung", expense.getDescription(), "Beschreibung");
                descriptionField.setWidthFull();
                descriptionField.setReadOnly(true);
                NumberField totalPriceField = new NumberField("Preis insgesamt");
                totalPriceField.setValue(expense.getTotalPrice());
                totalPriceField.setWidthFull();
                totalPriceField.setReadOnly(true);
                content.add(titelField, descriptionField, totalPriceField);
                content.setDefaultHorizontalComponentAlignment(Alignment.START);
            }
            if (event.getSelectedTab().equals(entryTab)) {
                Grid<TravelExpensesEntry> entryGrid = new Grid<>(TravelExpensesEntry.class, false);
                entryGrid.addClassName("travel-expense-grid");
                entryGrid.setItems(expense.getEntries());
                entryGrid.addColumn(TravelExpensesEntry::getTitel).setHeader("Titel").setAutoWidth(false);
                entryGrid.addColumn(entry -> DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm").format(entry.getFromDate())).setHeader("Startdatum").setAutoWidth(false);
                entryGrid.addColumn(entry -> DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm").format(entry.getEndDate())).setHeader("Enddatum").setAutoWidth(false);
                entryGrid.addColumn(TravelExpensesEntry::getPrice).setHeader("Preis").setAutoWidth(false);
                entryGrid.addColumn(createEntryGridButtons());
                content.add(entryGrid);
            }
        });
        tabs.add(detailsTab,entryTab);
        tabs.setSelectedTab(detailsTab);
        layout.add(tabs,content);
        layout.setHorizontalComponentAlignment(Alignment.CENTER,tabs);
    };

    public OwnTravelExpenseIndex(ITravelService travelService, AuthenticatedUser authenticatedUser, TravelExpensesEntryComp entryComp){
        _authenticatedUser = authenticatedUser;
        _travelService = travelService;
        invokingUser = authenticatedUser.get();
        _entryComp = entryComp;
        this.setSizeFull();
        prepareComponents();
        addComponents();
    }

    private void addComponents() {
        this.add(expensesGrid);
    }

    private void prepareComponents() {
        prepareGrid();
    }

    private void prepareGrid() {
        expensesGrid = new Grid<>(TravelExpenses.class, false);
        expensesGrid.setId("own-expenses-grid");
        expensesGrid.setClassName("own-expenses-grid");
        invokingUser.ifPresent(user -> updateGrid(user.getTravelExpenses().stream().toList()));
        expensesGrid.addColumn(TravelExpenses::getTitel).setHeader("Titel").setAutoWidth(true);
        expensesGrid.addColumn(TravelExpenses::getTotalPrice).setHeader("Preis").setAutoWidth(true);
        expensesGrid.addColumn(travelExpense -> {
            if (travelExpense.isApproved()) return "bestätigt";
            return "nicht bestätigt";
        }).setHeader("bestätigt?").setAutoWidth(true);
        expensesGrid.setItemDetailsRenderer(createDetailsRenderer());
        expensesGrid.setDetailsVisibleOnClick(true);
        expensesGrid.setMinHeight(90, Unit.PERCENTAGE);
    }

    private ComponentRenderer<VerticalLayout, TravelExpenses> createDetailsRenderer() {
        return new ComponentRenderer<>(VerticalLayout::new, detailsComponents);
    }

    private ComponentRenderer<HorizontalLayout, TravelExpensesEntry> createEntryGridButtons(){
        return new ComponentRenderer<>(HorizontalLayout::new, entryGridButtons);
    }

    private void updateGrid(List<TravelExpenses> travelExpenses) {
        expensesGrid.setItems(travelExpenses);
        expensesGrid.getDataProvider().refreshAll();
    }

    private Optional<InvoiceDocument> getDocument(UUID id){
        return _travelService.findInvoiceDocByEntryId(id);
    }

    public void updateGrid(){
        invokingUser = _authenticatedUser.get();
        travelExpenses = _travelService.getByUserId(invokingUser.get().getId()).stream().toList();
        invokingUser.ifPresent(user -> updateGrid(travelExpenses));
    }
}
