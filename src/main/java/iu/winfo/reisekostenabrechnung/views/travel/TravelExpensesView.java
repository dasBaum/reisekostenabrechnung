package iu.winfo.reisekostenabrechnung.views.travel;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import iu.winfo.reisekostenabrechnung.helper.security.AuthenticatedUser;
import iu.winfo.reisekostenabrechnung.models.security.Privilege;
import iu.winfo.reisekostenabrechnung.models.security.Role;
import iu.winfo.reisekostenabrechnung.models.security.User;
import iu.winfo.reisekostenabrechnung.views.MainLayout;

import javax.annotation.security.RolesAllowed;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Route(value = "expenses", layout = MainLayout.class)
@PageTitle("Reisekostenabrechnungen")
@RolesAllowed("travelExpense")
public class TravelExpensesView extends VerticalLayout {
    private final AuthenticatedUser _authenticatedUser;
    private final TravelExpenseCreate travelExpenseCreate;
    private final OwnTravelExpenseIndex ownTravelExpenseIndex;

    private final AllTravelExpenses allTravelExpenses;
    private Tab viewOwnExpenses;
    private Tab createExpense;
    private Tab viewExpenses;
    private List<Privilege> userPrivileges;

    private final VerticalLayout content;
    Tabs tabs;

    public TravelExpensesView(AuthenticatedUser authenticatedUser, TravelExpenseCreate travelExpenseCreate, OwnTravelExpenseIndex ownTravelExpenseIndex, AllTravelExpenses allTravelExpenses) {
        this.travelExpenseCreate = travelExpenseCreate;
        this.ownTravelExpenseIndex = ownTravelExpenseIndex;
        this.allTravelExpenses = allTravelExpenses;
        this.setSizeFull();
        _authenticatedUser = authenticatedUser;
        content = new VerticalLayout();
        content.setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        content.setSpacing(false);
        content.setSizeFull();
        getUserPrivileges();
        prepareTabs();
        this.add(content);
    }

    private void getUserPrivileges() {
        Optional<User> invokingUser = getInvokingUser();
        if(invokingUser.isPresent()){
            userPrivileges = new ArrayList<>();
            for (Role userRole : invokingUser.get().getRoles()) {
                userPrivileges.addAll(userRole.getPrivileges().stream().filter(privilege -> !userPrivileges.contains(privilege)).toList());
            }
        }
    }

    private void prepareTabs() {
        tabs = new Tabs();
        tabs.addSelectedChangeListener(event -> setContent(event.getSelectedTab()));
        if (userPrivileges.stream().anyMatch(privilege -> privilege.getName().equals("travelCreate"))) {
            prepareOwnExpenseView();
            prepareCreateExpenseView();
            tabs.add(createExpense);
            tabs.add(viewOwnExpenses);
            tabs.setSelectedTab(viewOwnExpenses);
        }
        if (userPrivileges.stream().anyMatch(privilege -> privilege.getName().equals("travelProcess") || privilege.getName().equals("travelProcessAll") || privilege.getName().equals("supervisor"))) {
            prepareExpenseView();
            tabs.add(viewExpenses);
        }

        this.add(tabs);
        this.setHorizontalComponentAlignment(Alignment.CENTER, tabs);
    }

    private void setContent(Tab selectedTab) {
        content.removeAll();
        if (selectedTab.equals(createExpense)) {
            content.add(travelExpenseCreate);
        }
        if (selectedTab.equals(viewOwnExpenses)) {
            ownTravelExpenseIndex.updateGrid();
            content.add(ownTravelExpenseIndex);
        }
        if (selectedTab.equals(viewExpenses)) {
            allTravelExpenses.updateItems();
            content.add(allTravelExpenses);
        }
    }

    private void prepareExpenseView() {
        viewExpenses = new Tab("Abrechnungen");
    }

    private void prepareCreateExpenseView() {
        createExpense = new Tab("Abrechnung erstellen");
    }

    private void prepareOwnExpenseView() {
        viewOwnExpenses = new Tab("eigene Abrechnungen");
    }

    private Optional<User> getInvokingUser(){
        return _authenticatedUser.get();
    }
}
