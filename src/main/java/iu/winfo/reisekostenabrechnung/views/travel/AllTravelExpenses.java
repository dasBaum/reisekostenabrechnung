package iu.winfo.reisekostenabrechnung.views.travel;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.SerializableBiConsumer;
import com.vaadin.flow.spring.annotation.UIScope;
import iu.winfo.reisekostenabrechnung.helper.security.AuthenticatedUser;
import iu.winfo.reisekostenabrechnung.interfaces.ITravelService;
import iu.winfo.reisekostenabrechnung.models.security.Role;
import iu.winfo.reisekostenabrechnung.models.security.User;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpenses;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpensesEntry;
import iu.winfo.reisekostenabrechnung.views.components.travel.TravelExpenseEntryView;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@UIScope
@Component
public class AllTravelExpenses extends VerticalLayout {

    private Grid<TravelExpenses> travelExpensesGrid;
    private List<TravelExpenses> travelExpenses;
    private ITravelService travelService;

    private User invokingUser = null;
    private final SerializableBiConsumer<HorizontalLayout, TravelExpenses> gridButtons = ((layout, travelExpense) -> {
        Button approveButton = new Button("Bestätigen");
        Button declineButton = new Button("Ablehnen");
        approveButton.addThemeVariants(ButtonVariant.LUMO_SUCCESS);
        declineButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        approveButton.setIcon(new Icon(VaadinIcon.CHECK));
        declineButton.setIcon(new Icon(VaadinIcon.BAN));
        approveButton.addClickListener(event -> {
            travelExpense.setApproved(true);
            travelService.updateExpense(travelExpense);
            updateItems();
        });
        declineButton.addClickListener(event -> {
            travelExpense.setApproved(false);
            travelService.updateExpense(travelExpense);
            updateItems();
        });
        layout.add(approveButton, declineButton);
    });
    private TravelExpenseEntryView travelExpenseEntryView;
    private final SerializableBiConsumer<HorizontalLayout, TravelExpensesEntry> entryGridButtons = (layout, entry) -> {
        Button viewEntryButton = new Button();
        viewEntryButton.setIcon(new Icon("lumo", "eye"));
        viewEntryButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        viewEntryButton.addClickListener(event -> {
            travelExpenseEntryView.setTravelExpensesEntry(entry);
            travelExpenseEntryView.open();
        });
        layout.add(viewEntryButton);
    };

    private final SerializableBiConsumer<VerticalLayout, TravelExpenses> detailsComponents = (layout, expense) -> {
        Tabs tabs = new Tabs();
        layout.setSizeFull();
        VerticalLayout detailsLayout = new VerticalLayout();
        Tab entryTab = new Tab("Abrechnungsposition");
        Tab detailsTab = new Tab("Details");
        VerticalLayout content = new VerticalLayout();
        content.setSizeFull();
        tabs.addSelectedChangeListener(event -> {
            content.removeAll();
            if (event.getSelectedTab().equals(detailsTab)) {
                TextField titelField = new TextField("Titel", expense.getTitel(), "Titel");
                titelField.setWidthFull();
                titelField.setReadOnly(true);
                TextArea descriptionField = new TextArea("Beschreibung", expense.getDescription(), "Beschreibung");
                descriptionField.setWidthFull();
                descriptionField.setReadOnly(true);
                NumberField totalPriceField = new NumberField("Preis insgesamt");
                totalPriceField.setValue(expense.getTotalPrice());
                totalPriceField.setWidthFull();
                totalPriceField.setReadOnly(true);
                content.add(titelField, descriptionField, totalPriceField);
                content.setDefaultHorizontalComponentAlignment(Alignment.START);
            }
            if (event.getSelectedTab().equals(entryTab)) {
                Grid<TravelExpensesEntry> entryGrid = new Grid<>(TravelExpensesEntry.class, false);
                entryGrid.addClassName("travel-expense-grid");
                entryGrid.setItems(expense.getEntries());
                entryGrid.addColumn(TravelExpensesEntry::getTitel).setHeader("Titel").setAutoWidth(false);
                entryGrid.addColumn(entry -> DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm").format(entry.getFromDate())).setHeader("Startdatum").setAutoWidth(false);
                entryGrid.addColumn(entry -> DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm").format(entry.getEndDate())).setHeader("Enddatum").setAutoWidth(false);
                entryGrid.addColumn(TravelExpensesEntry::getPrice).setHeader("Preis").setAutoWidth(false);
                entryGrid.addColumn(createEntryGridButtons());
                content.add(entryGrid);
            }
        });
        tabs.add(detailsTab, entryTab);
        tabs.setSelectedTab(detailsTab);
        layout.add(tabs, content);
        layout.setHorizontalComponentAlignment(Alignment.CENTER, tabs);
    };

    public AllTravelExpenses(ITravelService travelService, TravelExpenseEntryView travelExpenseEntryView, AuthenticatedUser authenticatedUser) {
        this.travelService = travelService;
        this.travelExpenseEntryView = travelExpenseEntryView;
        if (authenticatedUser.get().isPresent()) {
            invokingUser = authenticatedUser.get().get();
        }
        this.setSizeFull();
        prepareGrid();
        updateItems();
        this.add(travelExpensesGrid);
    }

    public void updateItems() {
        travelExpenses = new ArrayList<>();
        boolean canSeeAll = false;
        boolean isSupervisor = false;
        for (Role role : invokingUser.getRoles()) {
            if (role.getName().equals("Admin")) {
                travelExpenses = travelService.listExpenses();
                canSeeAll = true;
                break;
            }
            if (role.getPrivileges().stream().anyMatch(privilege -> privilege.getName().equals("travelProcessAll"))) {
                travelExpenses = travelService.listExpenses();
                canSeeAll = true;
                break;
            }
            if (role.getPrivileges().stream().anyMatch(privilege -> privilege.getName().equals("supervisor"))) {
                isSupervisor = true;
                break;
            }
        }
        if (!canSeeAll && isSupervisor) {
            for (Role role : invokingUser.getRoles()) {
                for (User user : role.getUsers()) {
                    for (TravelExpenses expense : travelService.getByUserId(user.getId())) {
                        if (!travelExpenses.contains(expense)) travelExpenses.add(expense);
                    }
                }
            }
        }
        travelExpensesGrid.setItems(travelExpenses);
    }

    private void prepareGrid() {
        travelExpensesGrid = new Grid<>(TravelExpenses.class, false);
        travelExpensesGrid.setId("own-expenses-grid");
        travelExpensesGrid.setClassName("own-expenses-grid");
        travelExpensesGrid.setMinHeight(90, Unit.PERCENTAGE);
        travelExpensesGrid.addColumn(TravelExpenses::getTitel).setHeader("Titel").setAutoWidth(true);
        travelExpensesGrid.addColumn(travelExpense -> travelExpense.getUser().getEmail()).setHeader("Nutzer E-Mail").setAutoWidth(true);
        travelExpensesGrid.addColumn(TravelExpenses::getTotalPrice).setHeader("Preis").setAutoWidth(true);
        travelExpensesGrid.addColumn(travelExpense -> {
            if (travelExpense.isApproved()) return "bestätigt";
            return "nicht bestätigt";
        }).setHeader("bestätigt?").setAutoWidth(true);
        travelExpensesGrid.addColumn(renderGridButtons()).setAutoWidth(true);
        travelExpensesGrid.setItemDetailsRenderer(createDetailsRenderer());
    }

    private ComponentRenderer<HorizontalLayout, TravelExpenses> renderGridButtons() {
        return new ComponentRenderer<>(HorizontalLayout::new, gridButtons);
    }

    private ComponentRenderer<VerticalLayout, TravelExpenses> createDetailsRenderer() {
        return new ComponentRenderer<>(VerticalLayout::new, detailsComponents);
    }

    private ComponentRenderer<HorizontalLayout, TravelExpensesEntry> createEntryGridButtons() {
        return new ComponentRenderer<>(HorizontalLayout::new, entryGridButtons);
    }
}
