package iu.winfo.reisekostenabrechnung.views.login;

import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@PageTitle("Login")
@Route(value = "login")
public class LoginView extends LoginOverlay {
    public LoginView() {
        setAction("login");

        LoginI18n i18n = LoginI18n.createDefault();
        LoginI18n.Form form = i18n.getForm();
        form.setTitle(null);
        form.setUsername("Nutzername");
        form.setPassword("Passwort");
        form.setForgotPassword("Passwort vergessen");
        i18n.setForm(form);
        i18n.setHeader(new LoginI18n.Header());
        i18n.getHeader().setTitle("RKA Login");
        i18n.setAdditionalInformation(null);
        setI18n(i18n);
        setError(true);
        setForgotPasswordButtonVisible(false);
        setOpened(true);
    }

}
