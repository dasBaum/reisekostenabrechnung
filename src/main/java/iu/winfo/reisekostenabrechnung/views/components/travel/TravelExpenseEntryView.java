package iu.winfo.reisekostenabrechnung.views.components.travel;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpensesEntry;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SpringComponent
@UIScope
public class TravelExpenseEntryView extends Dialog {

    private final Binder<TravelExpensesEntry> binder = new Binder<>(TravelExpensesEntry.class);
    private TravelExpensesEntry travelExpensesEntry;
    private TextField titelField;
    private TextArea descriptionField;
    private NumberField priceField;
    private DateTimePicker startDateField;
    private DateTimePicker endDateField;
    private Label downloadLabel;
    private Component documentComponent;

    public TravelExpenseEntryView() {
        setTravelExpensesEntry(new TravelExpensesEntry());
    }

    private VerticalLayout createComponents() {
        VerticalLayout layout = new VerticalLayout();
        prepareComponents();
        layout.add(titelField, descriptionField, priceField, startDateField, endDateField, downloadLabel, documentComponent);
        return layout;
    }

    private void prepareComponents() {
        titelField = new TextField("Titel");
        titelField.setWidthFull();
        descriptionField = new TextArea("Beschreibung");
        descriptionField.setWidthFull();
        priceField = new NumberField("Preis");
        priceField.setWidthFull();
        downloadLabel = new Label("Rechnung");
        prepareDateFields();
        prepareDownload();
    }

    private void prepareDownload() {
        if (this.travelExpensesEntry.getInvoiceDocument() != null) {
            String fileName = this.travelExpensesEntry.getInvoiceDocument().getFileName() + "." + this.travelExpensesEntry.getInvoiceDocument().getFileExtension();
            StreamResource streamResource = new StreamResource(fileName, () -> new ByteArrayInputStream(this.travelExpensesEntry.getInvoiceDocument().getBinaryData()));
            documentComponent = new Anchor(streamResource, "Hier die Rechnung downloaden!");
        } else {
            documentComponent = new H4("keine Rechnung hochgeladen");
        }
    }

    private void prepareDateFields() {
        ArrayList<String> months = new ArrayList<>(List.of("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"));
        ArrayList<String> daysShort = new ArrayList<>(List.of("So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"));
        ArrayList<String> days = new ArrayList<>(List.of("Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"));
        startDateField = new DateTimePicker("Startdatum");
        endDateField = new DateTimePicker("Enddatum");
        DatePicker.DatePickerI18n datePickerI18n = new DatePicker.DatePickerI18n();
        datePickerI18n.setCancel("Abbrechen");
        datePickerI18n.setFirstDayOfWeek(1);
        datePickerI18n.setToday("Heute");
        datePickerI18n.setWeek("Woche");
        datePickerI18n.setMonthNames(months);
        datePickerI18n.setWeekdays(days);
        datePickerI18n.setWeekdaysShort(daysShort);
        datePickerI18n.setDateFormat("dd.MM.yyyy");
        startDateField.setDatePickerI18n(datePickerI18n);
        endDateField.setDatePickerI18n(datePickerI18n);
        startDateField.setLocale(new Locale("de-DE"));
        endDateField.setLocale(new Locale("de-DE"));
        startDateField.setWidthFull();
        endDateField.setWidthFull();
    }

    public void setTravelExpensesEntry(TravelExpensesEntry travelExpensesEntry) {
        this.travelExpensesEntry = travelExpensesEntry;
        initComp();
        bindFields();
    }

    private void initComp() {
        this.removeAll();
        this.add(createComponents());
    }

    private void bindFields() {

        binder.forField(titelField).asRequired("Es muss ein Titel angegeben werden.").bind(TravelExpensesEntry::getTitel, TravelExpensesEntry::setTitel);
        binder.forField(descriptionField).asRequired("Es muss ein Beschreibung angegeben werden.").bind(TravelExpensesEntry::getDescription, TravelExpensesEntry::setDescription);
        binder.forField(priceField).asRequired("Es muss ein Preis angegeben werden.").bind(TravelExpensesEntry::getPrice, TravelExpensesEntry::setPrice);
        binder.forField(startDateField).asRequired("Es muss ein/e Start Datum und Uhrzeit angegeben werden.").bind(TravelExpensesEntry::getFromDate, TravelExpensesEntry::setFromDate);
        binder.forField(endDateField).asRequired("Es muss ein/e End Datum und Uhrzeit angegeben werden.").bind(TravelExpensesEntry::getEndDate, TravelExpensesEntry::setEndDate);
        binder.readBean(this.travelExpensesEntry);

    }
}
