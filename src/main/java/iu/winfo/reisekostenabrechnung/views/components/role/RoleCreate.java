package iu.winfo.reisekostenabrechnung.views.components.role;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.UIScope;
import iu.winfo.reisekostenabrechnung.dto.role.RoleSaveRequest;
import iu.winfo.reisekostenabrechnung.exceptions.RoleAlreadyExistsException;
import iu.winfo.reisekostenabrechnung.helper.privileges.PrivilegeMapper;
import iu.winfo.reisekostenabrechnung.helper.security.AuthenticatedUser;
import iu.winfo.reisekostenabrechnung.interfaces.IPrivilegeService;
import iu.winfo.reisekostenabrechnung.interfaces.IRoleService;
import iu.winfo.reisekostenabrechnung.models.security.Privilege;
import iu.winfo.reisekostenabrechnung.models.security.Role;
import iu.winfo.reisekostenabrechnung.models.security.User;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Component
@UIScope
public class RoleCreate extends Dialog {

    private final IRoleService _roleService;
    private final IPrivilegeService _privilegeService;
    private RoleSaveRequest role;
    private final AuthenticatedUser _authenticatedUser;

    private boolean userAllowedToCreateRole = false;
    private Optional<User> invokingUser = Optional.empty();
    private FormLayout formLayout;
    private IntegerField powerField;
    private TextField nameField;
    private Button createButton;
    private Button cancelButton;
    private Details privilegeDetails;
    private List<Checkbox> privilegeCheckboxes;
    private Binder<RoleSaveRequest> binder;

    public RoleCreate(IRoleService roleService,IPrivilegeService privilegeService, AuthenticatedUser authenticatedUser){
        _roleService = roleService;
        _privilegeService = privilegeService;
        _authenticatedUser = authenticatedUser;
        this.setModal(false);
        this.setDraggable(true);
    }

    public void initComponents(Role inputRole){
        setRole(inputRole);
        getInvokingUser();
        prepareFields();
        prepareButtons();
        prepareForm();
        addComponents();
        bindFields();
        setInputs();
    }

    private void setInputs() {
        binder.readBean(this.role);
    }

    private void setRole(Role inputRole) {
        role = new RoleSaveRequest();
        if(inputRole != null){
            role.setName(inputRole.getName());
            role.setPower(inputRole.getPower());
            role.setPrevRoleId(Optional.ofNullable(inputRole.getId()));
        }
    }

    private void prepareButtons() {
        createButton = new Button("Rolle erstellen");
        cancelButton = new Button("Abbrechen");

        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        createButton.addClickListener(e -> {
            try {
                createRole();
            }
            catch (Exception ex){

            }
        });
        cancelButton.addClickListener(e -> this.close());

        cancelButton.addClickShortcut(Key.ESCAPE);
        createButton.addClickShortcut(Key.ENTER);
    }

    private void createRole() throws ValidationException {
        if(binder.isValid() && userAllowedToCreateRole){
            binder.writeBean(role);
            role.setPrivilegeList(getSelectedPrivileges());
            try {
                Role updatedRole = _roleService.update(role);
                if(updatedRole != null){
                    fireEvent(new RoleCreate.SavedEvent(this,updatedRole));
                }
            }
            catch (RoleAlreadyExistsException ex){
                Notification notification = Notification.show("Rollen Name bereits vergeben.", 5000,Notification.Position.BOTTOM_START);
                notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
            }
        }
        else {
            Notification notification = Notification.show("Der Nutzer darf keine Anderen Rollen anlegen.", 5000,Notification.Position.BOTTOM_START);
            notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
        }
    }

    private Set<Privilege> getSelectedPrivileges() {
        Set<Privilege> privileges = new HashSet<>();
        for(Checkbox checkbox : privilegeCheckboxes){
            if(checkbox.getValue()){
                if(checkbox.getId().isPresent()){
                    Optional<Privilege> privilege = _privilegeService.get(UUID.fromString(checkbox.getId().get()));
                    privilege.ifPresent(privileges::add);
                }
            }
        }
        return privileges;
    }

    private void prepareFields() {
        powerField = new IntegerField();
        powerField.setMin(0);
        powerField.setMax(100);
        powerField.setHasControls(true);
        powerField.setLabel("Rollen Power");

        nameField = new TextField();
        nameField.setLabel("Rollen Name");
    }

    @Override
    public void close(){
        this.remove(formLayout);
        this.formLayout = null;
        super.close();
    }

    private void addComponents() {
        this.add(formLayout);
    }

    private void prepareForm() {
        formLayout = new FormLayout();
        H3 caption = new H3("Role bearbeiten");
        preparePrivilegeDetails();
        formLayout.add(caption);
        formLayout.add(nameField,powerField);
        formLayout.add(privilegeDetails);
        HorizontalLayout buttonLayout = new HorizontalLayout(createButton,cancelButton);
        formLayout.add(buttonLayout);
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("670px", 2));
        formLayout.setColspan(nameField,2);
        formLayout.setColspan(powerField,2);
        formLayout.setColspan(privilegeDetails,2);
        formLayout.setColspan(caption, 2);
        formLayout.setColspan(buttonLayout,2);
    }

    private void preparePrivilegeDetails() {
        privilegeDetails = new Details();
        List<String> rolePrivileges = new ArrayList<>();
        Optional<Role> formRole = Optional.empty();
        if(role.getPrevRoleId().isPresent()){
            formRole = _roleService.get(role.getPrevRoleId().get());
        }
        if(formRole.isPresent()){
            rolePrivileges = formRole.get().getPrivileges().stream().map(p -> p.getId().toString()).toList();
        }
        privilegeCheckboxes = new ArrayList<>();

        List<Privilege> privilegeList = _privilegeService.list();
        Optional<Privilege> travelExpensesPrivilege = privilegeList.stream().filter(privilege -> privilege.getName().equals("travelExpense")).findFirst();
        travelExpensesPrivilege.ifPresent(privilegeList::remove);
        for(Privilege privilege : privilegeList){
            Checkbox checkbox = new Checkbox();
            checkbox.setId(privilege.getId().toString());
            checkbox.setLabel(PrivilegeMapper.getDisplayText(privilege));
            if(rolePrivileges.stream().anyMatch(p -> p.equals(privilege.getId().toString()))){
                checkbox.setValue(true);
            }
            privilegeCheckboxes.add(checkbox);
        }
        for(Checkbox checkbox : privilegeCheckboxes) privilegeDetails.addContent(checkbox);
        privilegeDetails.setSummaryText("Privilegien");
        privilegeDetails.setOpened(true);
        privilegeDetails.setMinWidth("350px");
        privilegeDetails.setMaxWidth("350px");
        privilegeDetails.setHeightFull();
    }

    private void bindFields(){
        binder = new Binder<>(RoleSaveRequest.class);
        binder.forField(nameField).asRequired("Es muss ein Name angegeben werden").withValidator(name -> name.length() > 2,"Der Name muss länger als zwei Zeichen sein").bind(RoleSaveRequest::getName, RoleSaveRequest::setName);
        binder.forField(powerField).asRequired("Es muss ein Power Level angegeben werden").withValidator(power -> power > 0,"Das Power Level muss höher als 0 sein").bind(RoleSaveRequest::getPower, RoleSaveRequest::setPower);
    }

    public void getInvokingUser() {
        this.invokingUser = _authenticatedUser.get();
        if(invokingUser.isEmpty()) return;
        for (Role role : invokingUser.get().getRoles()) {
            if (role.getPrivileges().stream().anyMatch(privilege -> privilege.getName().equals("userWrite")))
                userAllowedToCreateRole = true;
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener){
        return getEventBus().addListener(eventType, listener);
    }

    public static abstract class RoleCreateEvent extends ComponentEvent<RoleCreate>{
        private final Role role;
        protected RoleCreateEvent(RoleCreate source, Role role){
            super(source,false);
            this.role = role;
        }
        public Role getRole(){
            return this.role;
        }
    }

    public static class SavedEvent extends RoleCreate.RoleCreateEvent {
        SavedEvent(RoleCreate source, Role role){
            super(source, role);
        }
    }

}
