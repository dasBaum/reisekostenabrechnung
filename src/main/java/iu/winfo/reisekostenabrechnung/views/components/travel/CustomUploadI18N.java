package iu.winfo.reisekostenabrechnung.views.components.travel;

import com.vaadin.flow.component.upload.UploadI18N;

import java.util.Arrays;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public class CustomUploadI18N extends UploadI18N {
    public CustomUploadI18N() {
        setDropFiles(new DropFiles()
                .setOne("Hier Datei reinziehen...   ")
                .setMany("Drop files here"));
        setAddFiles(new AddFiles()
                .setOne("Datei hochladen...")
                .setMany("Upload Files..."));
        setError(new Error()
                .setTooManyFiles("Zu viele Dateien")
                .setFileIsTooBig("Datei ist zu groß")
                .setIncorrectFileType("Incorrect File Type."));
        setUploading(new Uploading()
                .setStatus(new Uploading.Status()
                        .setConnecting("Connecting...")
                        .setStalled("Stalled")
                        .setProcessing("Processing File...")
                        .setHeld("Queued"))
                .setRemainingTime(new Uploading.RemainingTime()
                        .setPrefix("remaining time: ")
                        .setUnknown("unknown remaining time"))
                .setError(new Uploading.Error()
                        .setServerUnavailable("Upload failed, please try again later")
                        .setUnexpectedServerError("Upload failed due to server error")
                        .setForbidden("Upload forbidden")));
        setUnits(new Units()
                .setSize(Arrays.asList("B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")));
    }
}
