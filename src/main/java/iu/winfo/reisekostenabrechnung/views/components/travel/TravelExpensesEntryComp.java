package iu.winfo.reisekostenabrechnung.views.components.travel;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.UIScope;
import iu.winfo.reisekostenabrechnung.helper.security.AuthenticatedUser;
import iu.winfo.reisekostenabrechnung.models.documents.InvoiceDocument;
import iu.winfo.reisekostenabrechnung.models.security.User;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpensesEntry;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Component("entryCreateComp")
@UIScope
public class TravelExpensesEntryComp extends Dialog {

    private boolean updateEntry = false;
    private final Binder<TravelExpensesEntry> binder = new Binder<>(TravelExpensesEntry.class);
    private MemoryBuffer fileReceiver;
    private InvoiceDocument document;
    private final Optional<User> invokingUser;
    private TravelExpensesEntry travelExpensesEntry;

    private VerticalLayout uploadLayout;

    private TextField titel;

    private TextArea description;

    private NumberField price;

    private DateTimePicker fromDate;

    private DateTimePicker endDate;

    Select<String> select;

    private Button saveButton;

    private Button cancelButton;
    private Upload invoiceFileField;

    public TravelExpensesEntryComp(AuthenticatedUser authenticatedUser) {
        invokingUser = authenticatedUser.get();
        prepComponents();
        addComponents();
    }

    private void addComponents() {
        this.add(titel, description, price, fromDate, endDate, select, uploadLayout);
        HorizontalLayout buttonLayout = new HorizontalLayout(saveButton, cancelButton);
        buttonLayout.getStyle().set("flex-wrap", "wrap");
        buttonLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        this.add(buttonLayout);
    }

    private void prepComponents() {
        prepTitelField();
        prepDescriptionField();
        prepDateFields();
        prepPriceField();
        prepareSelect();
        prepUpload();
        prepButtons();
    }

    private void prepareSelect() {
        select = new Select<>("Inlandsreise", "EU-Ausland (25€)", "nicht EU-Ausland (50€)");
        select.setLabel("Reiseart");
        select.setWidthFull();
        select.addValueChangeListener(event -> {
            switch (event.getValue()) {
                case "Inlandsreise":
                    break;
                case "EU-Ausland (25€)":
                    break;
                case "nicht EU-Ausland (50€)":
                    break;
            }
        });
    }

    private void prepButtons() {
        saveButton = new Button("Speichern");
        cancelButton = new Button("Abbrechen");
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        saveButton.getStyle().set("margin-inline-end", "auto");
        saveButton.addClickListener(event -> {
            try {
                if (binder.isValid()) {
                    binder.writeBean(travelExpensesEntry);
                    saveEntry();
                }
            } catch (ValidationException e) {
                throw new RuntimeException(e);
            }
        });
        cancelButton.addClickListener(event -> this.close());
    }

    private void saveEntry() {
        if(document != null){
            if(travelExpensesEntry.getInvoiceDocument() != null){
                travelExpensesEntry.getInvoiceDocument().setFileSize(document.getFileSize());
                travelExpensesEntry.getInvoiceDocument().setFileName(document.getFileName());
                travelExpensesEntry.getInvoiceDocument().setFileType(document.getFileType());
                travelExpensesEntry.getInvoiceDocument().setFileExtension(document.getFileExtension());
                travelExpensesEntry.getInvoiceDocument().setBinaryData(document.getBinaryData());
            }
            else {
                travelExpensesEntry.setInvoiceDocument(document);
            }


        }
        if(!updateEntry){
            fireEvent(new TravelExpensesEntryComp.SaveEvent(this,travelExpensesEntry));
        }
        else {
            fireEvent(new TravelExpensesEntryComp.UpdateEvent(this,travelExpensesEntry));
        }
        invoiceFileField.clearFileList();
        this.close();
    }

    private void prepDateFields() {
        ArrayList<String> months = new ArrayList<>(List.of("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"));
        ArrayList<String> daysShort = new ArrayList<>(List.of("So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"));
        ArrayList<String> days = new ArrayList<>(List.of("Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"));
        fromDate = new DateTimePicker("Startdatum");
        endDate = new DateTimePicker("Enddatum");
        DatePicker.DatePickerI18n datePickerI18n = new DatePicker.DatePickerI18n();
        datePickerI18n.setCancel("Abbrechen");
        datePickerI18n.setFirstDayOfWeek(1);
        datePickerI18n.setToday("Heute");
        datePickerI18n.setWeek("Woche");
        datePickerI18n.setMonthNames(months);
        datePickerI18n.setWeekdays(days);
        datePickerI18n.setWeekdaysShort(daysShort);
        datePickerI18n.setDateFormat("dd.MM.yyyy");
        fromDate.setDatePickerI18n(datePickerI18n);
        endDate.setDatePickerI18n(datePickerI18n);
        fromDate.setLocale(new Locale("de-DE"));
        endDate.setLocale(new Locale("de-DE"));
        fromDate.setWidthFull();
        endDate.setWidthFull();
    }

    private void prepUpload() {
        fileReceiver = new MemoryBuffer();
        uploadLayout = new VerticalLayout();
        H4 title = new H4("Rechnung hochladen");
        title.getStyle().set("margin-top", "0");
        Paragraph hint = new Paragraph("Akzeptierte Datei Formate: PDF (.pdf) Word (.docx) JPEG (.jpg/.jpeg) PNG (.png)");
        hint.getStyle().set("color", "var(--lumo-secondary-text-color)");
        invoiceFileField = new Upload(fileReceiver);
        invoiceFileField.setMaxFileSize(32 * 1024 * 1024);
        invoiceFileField.setAcceptedFileTypes("application/pdf", ".pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", ".docx", "image/jpeg", ".jpg", ".jpeg", "image/png", ".png");
        invoiceFileField.addFileRejectedListener(event -> {
            String errorMessage = event.getErrorMessage();

            Notification notification = Notification.show(
                    errorMessage,
                    5000,
                    Notification.Position.MIDDLE
            );
            notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
        });
        CustomUploadI18N i18n = new CustomUploadI18N();
        i18n.getAddFiles().setOne("Datei hochladen...");
        i18n.getDropFiles().setOne("Hier Datei reinziehen");
        i18n.getError()
                .setIncorrectFileType(
                        "Die angegebene Datei ist nicht gültig. Bitte lad eine gültige Datei hoch.");
        invoiceFileField.setI18n(i18n);
        invoiceFileField.addSucceededListener(event -> {
            document = new InvoiceDocument();
            if(invokingUser.isEmpty()) return;
            document.setUploadedBy(invokingUser.get());
            try {
                byte[] data = fileReceiver.getInputStream().readAllBytes();
                document.setBinaryData(data);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            document.setFileExtension(FilenameUtils.getExtension(fileReceiver.getFileName()));
            document.setFileName(FilenameUtils.removeExtension(fileReceiver.getFileName()));
            document.setFileSize(event.getContentLength());
            document.setFileType(fileReceiver.getFileData().getMimeType());
            travelExpensesEntry.setInvoiceDocument(document);
            document.setTravelExpensesEntry(travelExpensesEntry);
        });
        uploadLayout.add(title, hint, invoiceFileField);
    }

    private void prepTitelField() {
        titel = new TextField("Titel");
        titel.setWidthFull();
    }

    public void setTravelExpensesEntry(TravelExpensesEntry entry) {
        travelExpensesEntry = entry;
        bindFields();
    }

    private void bindFields() {
        binder.forField(titel).asRequired("Es muss ein Titel angegeben werden.").withValidator(titelVal -> titelVal.length() >= 4,"Der Titel muss mindestens 4 Zeichen lang sein.").bind(TravelExpensesEntry::getTitel,TravelExpensesEntry::setTitel);
        binder.forField(description).asRequired("Es muss ein Beschreibung angegeben werden.").withValidator(descriptionVal -> descriptionVal.length() >= 4,"Die Beschreibung muss mindestens 4 Zeichen lang sein.").bind(TravelExpensesEntry::getDescription,TravelExpensesEntry::setDescription);
        binder.forField(price).asRequired("Es muss ein Preis angegeben werden.").withValidator(priceVal -> priceVal >= 0, "Der Preis darf nicht Null sein.").bind(TravelExpensesEntry::getPrice,TravelExpensesEntry::setPrice);
        binder.forField(fromDate).asRequired("Es muss ein/e Start Datum und Uhrzeit angegeben werden.").bind(TravelExpensesEntry::getFromDate,TravelExpensesEntry::setFromDate);
        binder.forField(endDate).asRequired("Es muss ein/e End Datum und Uhrzeit angegeben werden.").withValidator(endDateVal -> endDateVal.isAfter(fromDate.getValue()),"Das Enddatum muss nachdem Startdatum sein.").bind(TravelExpensesEntry::getEndDate, TravelExpensesEntry::setEndDate);

        binder.readBean(this.travelExpensesEntry);
    }

    @Override
    public void close() {
        super.close();
        this.document = null;
        this.travelExpensesEntry = null;
    }

    private void prepPriceField() {
        price = new NumberField("Preis");
        price.setWidthFull();
    }

    private void prepDescriptionField() {
        description = new TextArea("Beschreibung");
        description.setMaxLength(255);
        description.setValueChangeMode(ValueChangeMode.EAGER);
        description.addValueChangeListener(e -> e.getSource().setHelperText(e.getValue().length() + "/" + 255));
        description.setWidthFull();
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }

    public boolean getUpdateEntry() {
        return this.updateEntry;
    }

    public static class SaveEvent extends EntryCompEvent {
        SaveEvent(TravelExpensesEntryComp source, TravelExpensesEntry entry) {
            super(source, entry);
        }
    }

    public void setUpdateEntry(boolean updateEntry) {
        this.updateEntry = updateEntry;
    }

    public static abstract class EntryCompEvent extends ComponentEvent<TravelExpensesEntryComp> {
        private final TravelExpensesEntry entry;

        protected EntryCompEvent(TravelExpensesEntryComp source, TravelExpensesEntry entry) {
            super(source, false);
            this.entry = entry;
        }

        public TravelExpensesEntry getEntry() {
            return this.entry;
        }
    }


    public static class UpdateEvent extends EntryCompEvent {
        UpdateEvent(TravelExpensesEntryComp source, TravelExpensesEntry entry) {
            super(source, entry);
        }
    }

}
