package iu.winfo.reisekostenabrechnung.views.components.user;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import iu.winfo.reisekostenabrechnung.dto.user.UserSaveRequest;
import iu.winfo.reisekostenabrechnung.exceptions.UserAlreadyExistsException;
import iu.winfo.reisekostenabrechnung.helper.security.AuthenticatedUser;
import iu.winfo.reisekostenabrechnung.interfaces.IRoleService;
import iu.winfo.reisekostenabrechnung.interfaces.IUserService;
import iu.winfo.reisekostenabrechnung.models.security.Role;
import iu.winfo.reisekostenabrechnung.models.security.User;

import java.util.*;
/**
 * @author Erik Micheel
 * @version 1.0
 */
@SpringComponent
@UIScope
public class UserCreate extends Dialog {

    private final IUserService _userService;
    private final IRoleService _roleService;
    private final AuthenticatedUser _authenticatedUser;
    private UserSaveRequest user;
    private Optional<User> invokingUser = Optional.empty();
    private boolean isAllowedToCreateUser = false;
    private FormLayout formLayout;
    private TextField usernameField;
    private TextField emailField;
    private TextField firstNameField;
    private TextField lastNameField;
    private PasswordField passwordField;
    private PasswordField passwordMatchesField;
    private Button createUserButton;
    private Button cancelButton;
    private Binder<UserSaveRequest> binder;
    private HorizontalLayout controlButtonGroup;
    private List<Checkbox> rolesCheckboxes;

    private Details rolesDetails;

    public UserCreate(IUserService userService, IRoleService roleService, AuthenticatedUser authenticatedUser) {
        _userService = userService;
        _roleService = roleService;
        _authenticatedUser = authenticatedUser;
        this.setModal(false);
        this.setDraggable(true);
    }

    private void prepareRoleDetails() {
        rolesDetails = new Details();
        List<String> userRoles = new ArrayList<>();
        Optional<User> formUser = Optional.empty();
        if (user.getUserId().isPresent()) {
            formUser = _userService.get(user.getUserId().get());
        }
        if (formUser != null && formUser.isPresent()) {
            userRoles = formUser.get().getRoles().stream().map(r -> r.getId().toString()).toList();
        }
        rolesCheckboxes = new ArrayList<>();
        for (Role role : _roleService.list()) {
            Checkbox checkbox = new Checkbox();
            checkbox.setId(role.getId().toString());
            checkbox.setLabel(role.getName());
            if (userRoles.stream().anyMatch(r -> r.equals(role.getId().toString()))) {
                checkbox.setValue(true);
            }
            rolesCheckboxes.add(checkbox);
        }
        for (Checkbox checkbox : rolesCheckboxes) {
            rolesDetails.addContent(checkbox);
        }
        rolesDetails.setSummaryText("Rollen");
        rolesDetails.setOpened(true);
    }

    public void prepareUserDialog(User inputUser) {
        prepareControlButtonGroup();
        prepareFields();
        setUser(inputUser);
        prepareRoleDetails();
        prepareForm();
        add(formLayout);
    }

    private void prepareFields() {
        usernameField = new TextField("Nutzername");
        emailField = new TextField("E-Mail Adresse");
        firstNameField = new TextField("Vorname");
        lastNameField = new TextField("Nachname");
        passwordField = new PasswordField("Passwort");
        passwordMatchesField = new PasswordField("Passwort bestätigen");
    }

    private void prepareForm() {
        formLayout = new FormLayout();
        H3 caption = new H3("Nutzer bearbeiten");
        formLayout.add(caption,
                firstNameField, lastNameField,
                emailField,
                usernameField,
                passwordField, passwordMatchesField);
        formLayout.add(rolesDetails);
        formLayout.add(controlButtonGroup);
        formLayout.setResponsiveSteps(new ResponsiveStep("0", 1),
                new ResponsiveStep("670px", 2));
        formLayout.setColspan(caption, 2);
        formLayout.setColspan(rolesDetails, 2);
    }

    private void prepareControlButtonGroup() {
        HorizontalLayout buttonLayout = new HorizontalLayout();
        prepareButtons();
        buttonLayout.add(createUserButton, cancelButton);
        controlButtonGroup = buttonLayout;
    }

    private void prepareButtons() {
        createUserButton = new Button("Speichern");
        cancelButton = new Button("Abbrechen");

        createUserButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        createUserButton.addClickShortcut(Key.ENTER);
        cancelButton.addClickShortcut(Key.ESCAPE);

        createUserButton.addClickListener(e -> {
            try {
                createUser();
            } catch (ValidationException ex) {
                throw new RuntimeException(ex);
            }
        });
        cancelButton.addClickListener(e -> this.close());
    }

    private void createUser() throws ValidationException {
        if (binder.isValid() && isAllowedToCreateUser) {
            binder.writeBean(user);
            user.setRoleList(getSelectedRoles());
            try {
                User updatedUser = _userService.update(user);
                if (updatedUser != null) {
                    fireEvent(new UserCreate.SavedEvent(this, updatedUser));
                }
            }
            catch (UserAlreadyExistsException userAE){
                Notification notification = Notification.show("E-Mail Adresse bereits vergeben.", 5000,Notification.Position.BOTTOM_START);
                notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
            }
        }
        else {
            Notification notification = Notification.show("Der Nutzer darf keine Anderen Nutzer anlegen.", 5000,Notification.Position.BOTTOM_START);
            notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
        }
    }

    private Set<Role> getSelectedRoles() {
        Set<Role> roles = new HashSet<>();
        for (Checkbox checkbox : rolesCheckboxes) {
            if (checkbox.getValue()) {
                if(checkbox.getId().isPresent()) {
                    Optional<Role> role = _roleService.get(UUID.fromString(checkbox.getId().get()));
                    role.ifPresent(roles::add);
                }
            }
        }
        return roles;
    }

    public void getInvokingUser() {
        this.invokingUser = _authenticatedUser.get();
        if(invokingUser.isEmpty()) return;
        for (Role role : invokingUser.get().getRoles()) {
            if (role.getPrivileges().stream().anyMatch(privilege -> privilege.getName().equals("userWrite")))
                isAllowedToCreateUser = true;
        }
    }

    @Override
    public void close() {
        this.user = null;
        this.remove(formLayout);
        this.formLayout = null;
        super.close();
    }

    private void setUser(User inputUser) {
        user = new UserSaveRequest();
        if (inputUser != null) {
            user.setUserId(Optional.ofNullable(inputUser.getId()));
            user.setUsername(inputUser.getUsername());
            user.setEmail(inputUser.getEmail());
            user.setFirstName(inputUser.getFirstName());
            user.setLastName(inputUser.getLastName());
            user.setProfilePicture(inputUser.getProfilePictureUrl());
            user.setPassword("");
            user.setMatchingPassword("");
        }
        binder = new Binder<>(UserSaveRequest.class);
        bindFields();
        setInputs();
    }

    private void setInputs() {
        binder.readBean(this.user);
    }

    private void bindFields() {
        binder.forField(usernameField).asRequired("Es muss ein Nutzername angegeben werden.").withValidator(username -> username.length() >= 4, "Der Nutzername muss mindestens 4 Zeichen lang sein.").bind(UserSaveRequest::getUsername, UserSaveRequest::setUsername);
        binder.forField(emailField).asRequired("Es muss eine E-Mail angegeben werden.").withValidator(new EmailValidator("Es muss eine gültige E-Mail Adresse angegeben werden.")).bind(UserSaveRequest::getEmail, UserSaveRequest::setEmail);
        binder.forField(firstNameField).bind(UserSaveRequest::getFirstName, UserSaveRequest::setFirstName);
        binder.forField(lastNameField).bind(UserSaveRequest::getLastName, UserSaveRequest::setLastName);
        Binder.Binding<UserSaveRequest, String> passwordBind = binder.forField(passwordField).withValidator(password -> password.equals(passwordMatchesField.getValue()),"Die Passwörter stimmen nicht überein").bind(UserSaveRequest::getPassword, UserSaveRequest::setPassword);
        binder.forField(passwordMatchesField).bind(UserSaveRequest::getMatchingPassword, UserSaveRequest::setMatchingPassword);
        passwordField.addValueChangeListener(e -> passwordBind.validate());
        passwordMatchesField.addValueChangeListener(e -> passwordBind.validate());
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener){
        return getEventBus().addListener(eventType, listener);
    }

    public static abstract class UserCreateEvent extends ComponentEvent<UserCreate>{
        private final User user;
        protected UserCreateEvent(UserCreate source, User user){
            super(source,false);
            this.user = user;
        }
        public User getUser(){
            return this.user;
        }
    }

    public static class SavedEvent extends UserCreate.UserCreateEvent {
        SavedEvent(UserCreate source, User user){
            super(source, user);
        }
    }
}
