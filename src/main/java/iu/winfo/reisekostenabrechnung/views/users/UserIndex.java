package iu.winfo.reisekostenabrechnung.views.users;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.SerializableBiConsumer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import iu.winfo.reisekostenabrechnung.helper.security.AuthenticatedUser;
import iu.winfo.reisekostenabrechnung.interfaces.IUserService;
import iu.winfo.reisekostenabrechnung.models.security.Privilege;
import iu.winfo.reisekostenabrechnung.models.security.Role;
import iu.winfo.reisekostenabrechnung.models.security.User;
import iu.winfo.reisekostenabrechnung.views.MainLayout;
import iu.winfo.reisekostenabrechnung.views.components.user.UserCreate;

import javax.annotation.security.RolesAllowed;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Route(value = "users", layout = MainLayout.class)
@PageTitle("Nutzer verwalten")
@RolesAllowed("userRead")
public class UserIndex extends VerticalLayout {

    private UserCreate _userCreate;
    private Optional<User> invokingUser;
    private IUserService _userService;
    private final SerializableBiConsumer<HorizontalLayout, User> updateGridButtonsConsumer = (layout, user) -> {
        if (invokingUser.isEmpty()) return;
        List<String> privileges = new ArrayList<>();
        for (Role role: invokingUser.get().getRoles()) {
            privileges.addAll(role.getPrivileges().stream().map(Privilege::getName).filter(name -> !privileges.contains(name)).toList());
        }
        Button updateButton = new Button(new Icon(VaadinIcon.COG_O));

        updateButton.addClickListener(e -> {
            _userCreate.getInvokingUser();
            _userCreate.prepareUserDialog(user);
            _userCreate.open();
        });
        updateButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button deleteButton = new Button(new Icon(VaadinIcon.TRASH));
        deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        deleteButton.addClickListener(e -> {
            if (!user.equals(invokingUser.get())) {
                _userService.delete(user.getId());
                updateGrid();
            }
        });
        if (privileges.stream().anyMatch(privilege -> privilege.equals("userWrite")) && privileges.stream().anyMatch(privilege -> privilege.equals("userDelete"))) {
            layout.add(updateButton, deleteButton);
        } else if (privileges.stream().anyMatch(privilege -> privilege.equals("userWrite"))) {
            layout.add(updateButton);
        }
    };
    private Grid<User> grid;
    private Button createUserBtn;

    public UserIndex(IUserService userService, UserCreate userCreate, AuthenticatedUser authenticatedUser) {
        _userService = userService;
        _userCreate = userCreate;
        invokingUser = authenticatedUser.get();
        setSizeFull();
        prepareGrid();
        prepareButtons();
        addButtons();
        _userCreate.addListener(UserCreate.SavedEvent.class, e -> {
            updateGrid();
            _userCreate.close();
        });
        add(grid);
    }

    private ComponentRenderer<HorizontalLayout, User> createGridButtons() {
        return new ComponentRenderer<>(HorizontalLayout::new, updateGridButtonsConsumer);
    }

    private void addButtons() {
        if(invokingUser.isEmpty()) return;
        for (Role role : invokingUser.get().getRoles()) {
            if (role.getPrivileges().stream().anyMatch(privilege -> privilege.getName().equals("userWrite")))
                add(createUserBtn);
        }
    }

    private void prepareButtons() {
        createUserBtn = new Button("Nutzer erstellen", e -> {
            _userCreate.getInvokingUser();
            _userCreate.prepareUserDialog(new User());
            _userCreate.open();
        });
        createUserBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    private void prepareGrid() {
        grid = new Grid<>(User.class, false);
        grid.addClassName("user-grid");
        grid.addColumn(User::getUsername).setHeader("Nutzername").setAutoWidth(true);
        grid.addColumn(User::getEmail).setHeader("E-Mail").setAutoWidth(true);
        grid.addColumn(createGridButtons()).setAutoWidth(true);
        grid.setSizeFull();
        updateGrid();
    }

    private void updateGrid() {
        List<User> users = _userService.list();
        grid.setItems(users);
    }
}

