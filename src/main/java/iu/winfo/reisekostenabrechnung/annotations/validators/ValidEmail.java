package iu.winfo.reisekostenabrechnung.annotations.validators;

import iu.winfo.reisekostenabrechnung.validators.user.EmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Erik Micheel
 * @version 1.0
 * Quelle: https://stackoverflow.com/questions/71033592/how-to-say-this-field-has-email-annotation-from-outside
 */
@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
//Gibt an in welcher Klasse sich die Validierungslogik befindet
@Constraint(validatedBy = EmailValidator.class)
@Documented
public @interface ValidEmail {
    String message() default "invalid email";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
