package iu.winfo.reisekostenabrechnung.annotations.validators;

import iu.winfo.reisekostenabrechnung.validators.user.PasswordMatchesValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Eine Annotation die überprüft ob die Passwörter überein stimmen
 * @author Erik Micheel
 * @version 1.0
 *
 */


/**
 * @author Erik Micheel
 * @version 1.0
 */
@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
//Gibt an in welcher Klasse sich die Validierungslogik befindet
@Constraint(validatedBy = PasswordMatchesValidator.class)
@Documented
public @interface PasswordMatches {

    /**
     *
     * @return die Nachricht die zurückgegeben wird wenn Annotation erfüllt wird
     */
    String message() default "Passwords don't match";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
