package iu.winfo.reisekostenabrechnung.interfaces;

import iu.winfo.reisekostenabrechnung.dto.user.UserSaveRequest;
import iu.winfo.reisekostenabrechnung.exceptions.UserAlreadyExistsException;
import iu.winfo.reisekostenabrechnung.models.security.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public interface IUserService {
    Optional<User> get(UUID id);

    User update(User user);

    User update(UserSaveRequest user) throws UserAlreadyExistsException;

    void delete(UUID id);

    Page<User> list(Pageable pageable);

    List<User> list();

    long count();
}
