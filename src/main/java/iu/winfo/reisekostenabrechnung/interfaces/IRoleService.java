package iu.winfo.reisekostenabrechnung.interfaces;

import iu.winfo.reisekostenabrechnung.dto.role.RoleSaveRequest;
import iu.winfo.reisekostenabrechnung.exceptions.RoleAlreadyExistsException;
import iu.winfo.reisekostenabrechnung.models.security.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public interface IRoleService {
    Role update(Role role);
    Role update(RoleSaveRequest role) throws RoleAlreadyExistsException;
    Optional<Role> get(UUID id);
    Optional<Role> get(String name);

    void delete(UUID id);

    Page<Role> list(Pageable pageable);

    List<Role> list();

    long count();
}
