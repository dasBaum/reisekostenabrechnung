package iu.winfo.reisekostenabrechnung.interfaces;

import iu.winfo.reisekostenabrechnung.models.documents.InvoiceDocument;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpenses;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpensesEntry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public interface ITravelService {
    TravelExpenses updateExpense(TravelExpenses travelExpenses);
    Optional<TravelExpenses> findExpense(UUID id);
    void deleteExpense(UUID id);
    List<TravelExpenses> listExpenses();
    Page<TravelExpenses> listExpenses(Pageable pageable);

    TravelExpensesEntry updateEntry(TravelExpensesEntry expensesEntry);
    Optional<TravelExpensesEntry> findEntry(UUID id);
    void deleteEntry(UUID id);
    List<TravelExpensesEntry> listEntries();
    Page<TravelExpensesEntry> listEntries(Pageable pageable);

    Set<TravelExpensesEntry> getEntriesByExpense(UUID id);

    Set<TravelExpenses> getByUserId(UUID id);
    Optional<InvoiceDocument> findInvoiceDocByEntryId(UUID id);
}
