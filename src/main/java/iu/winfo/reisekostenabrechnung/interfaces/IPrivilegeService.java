package iu.winfo.reisekostenabrechnung.interfaces;

import iu.winfo.reisekostenabrechnung.models.security.Privilege;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import oshi.jna.platform.mac.SystemB;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public interface IPrivilegeService {

    Privilege update(Privilege privilege);

    Optional<Privilege> get(UUID id);

    Optional<Privilege> get(String name);

    void delete(UUID id);

    Page<Privilege> list(Pageable pageable);
    List<Privilege> list();

    long count();
}
