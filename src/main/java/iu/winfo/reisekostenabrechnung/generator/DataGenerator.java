package iu.winfo.reisekostenabrechnung.generator;

import com.vaadin.flow.spring.annotation.SpringComponent;
import iu.winfo.reisekostenabrechnung.interfaces.IPrivilegeService;
import iu.winfo.reisekostenabrechnung.interfaces.IRoleService;
import iu.winfo.reisekostenabrechnung.models.security.Privilege;
import iu.winfo.reisekostenabrechnung.models.security.Role;
import iu.winfo.reisekostenabrechnung.models.security.User;
import iu.winfo.reisekostenabrechnung.services.security.PrivilegeService;
import iu.winfo.reisekostenabrechnung.services.security.RoleService;
import iu.winfo.reisekostenabrechnung.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@SpringComponent
public class DataGenerator {

    private IPrivilegeService _privilegeService;
    private IRoleService _roleService;
    private Logger _logger;
    private Collection<Privilege> privileges;

    @Bean
    public CommandLineRunner loadData(PasswordEncoder passwordEncoder, UserService userService, RoleService roleService, PrivilegeService privilegeService) {
        return args -> {
            _privilegeService = privilegeService;
            _roleService = roleService;
            _logger = LoggerFactory.getLogger(getClass());
            //wenn bereits Nutzer angelegt wurden, wird die Methode hier beendet
            if (userService.count() != 0L) {
                _logger.info("Using existing database");
                return;
            }

            _logger.info("Generating demo data");

            _logger.info("... generating 2 User entities...");

            if (_privilegeService.count() == 0L) {
                _logger.info("prepare privileges");
                preparePrivilege();
            }

            if (_roleService.count() == 0L) {
                _logger.info("preparing roles");
                prepareRoles();
            }
            User user = new User();
            user.setUsername("user");
            user.setEmail("user@user.com");
            user.setPasswordHash(passwordEncoder.encode("user"));
            @SuppressWarnings("OptionalGetWithoutIsPresent") Role role = roleService.get("User").get();
            user.setRoles(Set.of(role));
            userService.update(user);
            User admin = new User();
            admin.setEmail("admin@admin.com");
            admin.setUsername("admin");
            admin.setPasswordHash(passwordEncoder.encode("admin"));
            //noinspection OptionalGetWithoutIsPresent
            role = _roleService.get("Admin").get();
            admin.setRoles(Set.of(role));
            userService.update(admin);

            _logger.info("Generated demo data");
        };
    }

    private void prepareRoles() {
        Role admin = new Role("Admin", 10);
        Role user = new Role("User", 1);
        Role supervisor = new Role("Vorgesetzter", 2);
        for (Privilege privilege : privileges) {
            if (privilege.getName().equals("supervisor")) {
                supervisor.addPrivilege(privilege);
                continue;
            }
            admin.addPrivilege(privilege);
        }
        _roleService.update(admin);
        _roleService.update(user);
        _roleService.update(supervisor);
    }

    private void preparePrivilege() {
        privileges = new ArrayList<>();
        ArrayList<String> privilegesNames = new ArrayList<>(List.of("userRead", "userWrite", "userDelete", "roleRead", "roleWrite", "roleDelete", "travelExpense", "travelCreate", "travelProcess", "travelProcessAll", "supervisor"));
        for (String privilege : privilegesNames) {
            privileges.add(new Privilege(privilege));
        }
        for (Privilege privilege : privileges) {
            _privilegeService.update(privilege);
        }
    }

}