package iu.winfo.reisekostenabrechnung.repositories;

import iu.winfo.reisekostenabrechnung.models.travel.TravelExpensesEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Repository
public interface TravelExpensesEntryRepository extends JpaRepository<TravelExpensesEntry, UUID> {
    Set<TravelExpensesEntry> findByTravelExpenses_IdEquals(UUID id);

}
