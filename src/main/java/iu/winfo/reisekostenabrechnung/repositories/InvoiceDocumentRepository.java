package iu.winfo.reisekostenabrechnung.repositories;

import iu.winfo.reisekostenabrechnung.models.documents.InvoiceDocument;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public interface InvoiceDocumentRepository extends JpaRepository<InvoiceDocument, UUID> {
    @Transactional
    Optional<InvoiceDocument> findByTravelExpensesEntry_IdEquals(UUID id);
}