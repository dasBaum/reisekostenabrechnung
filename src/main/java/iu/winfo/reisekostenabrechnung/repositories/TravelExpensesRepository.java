package iu.winfo.reisekostenabrechnung.repositories;

import iu.winfo.reisekostenabrechnung.models.travel.TravelExpenses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Set;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Repository
public interface TravelExpensesRepository extends JpaRepository<TravelExpenses, UUID> {

    @Query("SELECT SUM(entries.price) FROM TravelExpensesEntry entries where entries.travelExpenses.id = :id")
    double getTotalPriceByExpense(@Param("id")UUID id);

    @Transactional
    Set<TravelExpenses> findByUser_IdEquals(UUID id);

}
