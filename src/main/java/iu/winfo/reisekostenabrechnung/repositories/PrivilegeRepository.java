package iu.winfo.reisekostenabrechnung.repositories;

import iu.winfo.reisekostenabrechnung.models.security.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public interface PrivilegeRepository extends JpaRepository<Privilege, UUID> {
    Optional<Privilege> findByName(String name);
}
