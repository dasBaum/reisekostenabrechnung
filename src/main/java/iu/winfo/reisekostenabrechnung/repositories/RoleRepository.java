package iu.winfo.reisekostenabrechnung.repositories;

import iu.winfo.reisekostenabrechnung.models.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public interface RoleRepository extends JpaRepository<Role, UUID> {
    Optional<Role> findByName(String name);
}
