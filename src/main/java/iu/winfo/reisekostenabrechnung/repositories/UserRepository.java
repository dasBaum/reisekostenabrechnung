package iu.winfo.reisekostenabrechnung.repositories;

import iu.winfo.reisekostenabrechnung.models.security.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public interface UserRepository extends JpaRepository<User, UUID> {
    User findByUsername(String username);

    User findByEmail(String email);
}