package iu.winfo.reisekostenabrechnung.validators.user;

import iu.winfo.reisekostenabrechnung.annotations.validators.PasswordMatches;
import iu.winfo.reisekostenabrechnung.dto.user.UserSaveRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Erik Micheel
 * @version 1.0
 */
public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {
    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        UserSaveRequest user = (UserSaveRequest) obj;
        return user.getPassword().equals(user.getMatchingPassword());
    }
}