package iu.winfo.reisekostenabrechnung.models.travel;

import iu.winfo.reisekostenabrechnung.models.AbstractEntity;
import iu.winfo.reisekostenabrechnung.models.security.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Set;


/**
 * @author Erik Micheel
 * @version 1.0
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TravelExpenses extends AbstractEntity {

    @OneToMany(mappedBy = "travelExpenses", cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    private Set<TravelExpensesEntry> entries;

    @Column(nullable = true)
    private boolean approved;

    @Column(nullable = false, length = 50)
    private String titel;

    @Column(nullable = false)
    private String description;

    @Column
    private double totalPrice;

    @ManyToOne
    private User approvedBy;

    @ManyToOne
    private User user;
}
