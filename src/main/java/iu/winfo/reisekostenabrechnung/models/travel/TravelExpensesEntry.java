package iu.winfo.reisekostenabrechnung.models.travel;

import iu.winfo.reisekostenabrechnung.models.AbstractEntity;
import iu.winfo.reisekostenabrechnung.models.documents.InvoiceDocument;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TravelExpensesEntry extends AbstractEntity {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "file_id", referencedColumnName = "id")
    private InvoiceDocument invoiceDocument;

    @ManyToOne(cascade = {CascadeType.REFRESH, CascadeType.DETACH,CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "expenses_id")
    private TravelExpenses travelExpenses;

    @Column(nullable = false, length = 50)
    private String titel;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private double price;

    @Column(nullable = false)
    private LocalDateTime fromDate;

    @Column(nullable = false)
    private LocalDateTime endDate;
}
