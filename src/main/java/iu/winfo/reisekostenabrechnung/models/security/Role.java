package iu.winfo.reisekostenabrechnung.models.security;

import iu.winfo.reisekostenabrechnung.models.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Role extends AbstractEntity {


    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private int power;

    @ManyToMany(mappedBy = "roles")
    @Fetch(FetchMode.JOIN)
    @ToString.Exclude
    private Set<User> users;

    @ManyToMany
    @Fetch(FetchMode.JOIN)
    @JoinTable(name = "roles_privileges", joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "privilege_id", referencedColumnName = "id"))
    @ToString.Exclude
    private Set<Privilege> privileges;

    public Role(final String name, int power) {
        this.name = name;
        this.power = power;
    }

    public String toString() {
        return this.getName();
    }

    public void addUser(User user) {
        if (this.users == null) this.users = new HashSet<>();
        this.users.add(user);
    }

    public void addPrivilege(Privilege privilege) {
        if (this.privileges == null) this.privileges = new HashSet<>();
        this.privileges.add(privilege);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Role role = (Role) o;
        return getId() != null && Objects.equals(getId(), role.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}