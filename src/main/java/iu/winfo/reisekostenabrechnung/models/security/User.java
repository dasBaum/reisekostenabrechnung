package iu.winfo.reisekostenabrechnung.models.security;

import iu.winfo.reisekostenabrechnung.models.AbstractEntity;
import iu.winfo.reisekostenabrechnung.models.documents.InvoiceDocument;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpenses;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.*;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Entity
@Table(name = "application_user")
@Getter
@Setter
@ToString
public class User extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String username;
    private String firstName;
    private String lastName;
    @Column(nullable = false, unique = true)
    private String email;
    private String passwordHash;
    @ManyToMany
    @Fetch(FetchMode.JOIN)
    @JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    @ToString.Exclude
    private Set<Role> roles;

    @Lob
    @Type(type = "org.hibernate.type.ImageType")
    private byte[] profilePictureUrl;

    @OneToMany(mappedBy = "uploadedBy")
    @Fetch(FetchMode.JOIN)
    private Set<InvoiceDocument> uploadedDocuments;

    @OneToMany(mappedBy = "approvedBy")
    private Set<TravelExpenses> hadApproved;

    @OneToMany(mappedBy = "user")
    @Fetch(FetchMode.JOIN)
    private Set<TravelExpenses> travelExpenses;


    public User() {
        setUsername("");
        setEmail("");
        setFirstName("");
        setLastName("");
    }

    public void addRole(Role role) {
        if (this.roles == null) this.roles = new HashSet<>();
        this.roles.add(role);
        role.addUser(this);
    }

    public List<Privilege> getPrivileges() {
        List<Privilege> privileges = new ArrayList<>();
        for (Role role : this.getRoles()) {
            privileges.addAll(role.getPrivileges().stream().filter(privileges::contains).toList());
        }
        return privileges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;
        return getId() != null && Objects.equals(getId(), user.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
