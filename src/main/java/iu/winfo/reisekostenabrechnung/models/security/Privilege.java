package iu.winfo.reisekostenabrechnung.models.security;

import iu.winfo.reisekostenabrechnung.models.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Privilege extends AbstractEntity {
    @Column(nullable = false, unique = true)
    private String name;

    @ManyToMany(mappedBy = "privileges")
    @ToString.Exclude
    private Collection<Role> roles;

    public Privilege(final String name) {
        super();
        this.name = name;
    }

    public String toString() {
        return this.getName();
    }

    public void addRole(Role role) {
        if (this.roles == null) this.roles = new ArrayList<>();
        this.roles.add(role);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Privilege privilege = (Privilege) o;
        return this.getId() != null && Objects.equals(this.getId(), privilege.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}