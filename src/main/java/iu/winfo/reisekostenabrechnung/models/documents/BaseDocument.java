package iu.winfo.reisekostenabrechnung.models.documents;

import iu.winfo.reisekostenabrechnung.models.AbstractEntity;
import iu.winfo.reisekostenabrechnung.models.security.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@MappedSuperclass
@Getter
@Setter
@ToString
@NoArgsConstructor
public class BaseDocument extends AbstractEntity {

    @Column(nullable = false)
    private String fileName;

    @Column(nullable = false)
    private String fileType;

    @Column(nullable = false)
    private String fileExtension;

    @Column(nullable = false)
    private long fileSize;

    @Column
    @Lob
    private byte[] binaryData;


}
