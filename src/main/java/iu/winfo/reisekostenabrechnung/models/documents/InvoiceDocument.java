package iu.winfo.reisekostenabrechnung.models.documents;

import iu.winfo.reisekostenabrechnung.models.security.User;
import iu.winfo.reisekostenabrechnung.models.travel.TravelExpensesEntry;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class InvoiceDocument extends BaseDocument{

    @ManyToOne
    @JoinColumn(name = "uploaded_by_id", nullable = false, referencedColumnName = "id")
    private User uploadedBy;

    @OneToOne
    @JoinColumn(name = "entry_id", referencedColumnName = "id")
    private TravelExpensesEntry travelExpensesEntry;
}
