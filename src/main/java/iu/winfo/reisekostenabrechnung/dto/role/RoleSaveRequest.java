package iu.winfo.reisekostenabrechnung.dto.role;

import iu.winfo.reisekostenabrechnung.models.security.Privilege;
import lombok.Data;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Data
public class RoleSaveRequest {
    private String name;
    private int power;
    private Set<Privilege> privilegeList;

    //wenn das DTO genutzt wird um eine Rolle zu updaten, wird hier die ID der zu updatenen Rolle eingetragen
    private Optional<UUID> prevRoleId;
}
