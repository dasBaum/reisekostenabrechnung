package iu.winfo.reisekostenabrechnung.dto.user;

import iu.winfo.reisekostenabrechnung.annotations.validators.PasswordMatches;
import iu.winfo.reisekostenabrechnung.annotations.validators.ValidEmail;
import iu.winfo.reisekostenabrechnung.models.security.Role;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * @author Erik Micheel
 * @version 1.0
 */
@Data
@PasswordMatches
public class UserSaveRequest {

    @NotNull
    @NotEmpty
    @ValidEmail
    private String email;

    private String username;

    @NotNull
    @NotEmpty
    private String firstName;

    @NotNull
    @NotEmpty
    private String lastName;

    @NotNull
    @NotEmpty
    private String password;

    private String matchingPassword;

    private Optional<UUID> userId;

    private Set<Role> roleList;

    private byte[] profilePicture;

}

